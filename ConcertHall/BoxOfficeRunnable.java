package ConcertHall;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * BoxOfficeRunnable class This class creates our box office(s) by implementing
 * Runnable. The class consists of the theatre, the waiting line, and the name
 * of the specific box office (once the class is instantiated). The run method
 * allows the box offices (however many are created) to process and reserve
 * tickets concurrently
 * 
 * Karl Dedow and Kevin Rosen 
 * Section: W 10:30-12:00 
 * V1 12.7.2014
 */
public class BoxOfficeRunnable implements Runnable
{
	private Theatre theatre;
	private Queue<Person> line;
	private String name;

	/**
	 * 
	 * @param passedTheatre
	 *           the theatre configuration
	 * @param passedLine
	 *           the waiting line of people
	 * @param passedName
	 *           the name of the box office (e.g. Office A)
	 */
	public BoxOfficeRunnable(Theatre passedTheatre,
			ConcurrentLinkedQueue<Person> passedLine, String passedName)
	{
		theatre = passedTheatre;
		line = passedLine;
		name = passedName;
	}

	/**
	 * This method processes the queue line. While the line is still full, the
	 * next person in line is sent to a box office and reserved the next best
	 * available seat. The seat (row and seat number) is then printed to to the
	 * console and the seat is marked as unavailable. The box office(s) cease
	 * processing tickets once either the line empties or the theatre sells out
	 * of tickets (no more seats available)
	 */
	public void run()
	{
		// Process the queue line
		while (!line.isEmpty())
		{
			Person tempPerson = line.remove();
			Seat bestSeat = theatre.bestAvailableSeat(tempPerson);
			bestSeat = theatre.markAvailableSeatTaken(bestSeat, tempPerson);
			theatre.printTicketSeat(bestSeat, name);
			// If there are no more available seats, break out of loop
			if (bestSeat == null)
			{
				break;
			}
		}
	}
}
