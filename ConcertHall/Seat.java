package ConcertHall;

/**
 * The Seat class This class implements a seat. Attributes include row, number,
 * handicap status, availability, and the person occupying the seat (if
 * occupied).
 * 
 * Karl Dedow and Kevin Rosen 
 * Section: W 10:30-12:00 
 * V1 12.7.2014
 */
public class Seat
{
	private String row;
	private int number;
	private boolean handicap;
	private boolean available;
	private int id;
	private int block;
	private Person person;

	/**
	 * The constructor for the seat class. Sets the row, number, handicap status,
	 * overall id, availability (initially true), block (the block that the seat
	 * is in), and person (initiall null).
	 * 
	 * @param row
	 *           the row of the seat (e.g. A)
	 * @param number
	 *           the seat number
	 * @param handicap
	 *           boolean value that inidicates if seat is handicap
	 * @param id
	 *           the overall id of the seat
	 */
	public Seat(String row, int number, boolean handicap, int id)
	{
		this.row = row;
		this.number = number;
		this.handicap = handicap;
		this.id = id;
		available = true;
		block = calculateBlock();
		person = null;
	}

	/**
	 * This method calculates the block that the seat belongs in. Each block is a
	 * group of seats in a specific section of the theatre. Each seat is assigned
	 * a block number so that the next best available seat can be calculated. The
	 * first block is the most desirous seat section, followed by the second, and
	 * so on. The block number for the seat is used when creating the priority
	 * queue of available seats.
	 * 
	 * @return the block number associated with the seat
	 */
	private int calculateBlock()
	{
		int intVal = 0;
		int blockNum = 1;
		// Get the integer value of the seat's row
		for (int i = 0; i < row.length(); i++)
		{
			intVal += (int) row.charAt(i);
		}
		// First two blocks are rows A-F
		if (intVal <= 'F')
		{
			// generate two blocks
			return generateBlock(blockNum);
			// Blocks three and four are rows G-M (no row I)
		} else if (intVal <= 'M')
		{
			// generate two blocks
			return generateBlock(2 * blockNum + 1);
			// Blocks five and six are rows N-T (no row O)
		} else if (intVal <= 'T')
		{
			// generate two blocks
			return generateBlock(4 * blockNum + 1);
		}
		// All rows after row T are either in block seven or eight
		else
		{
			// generate two blocks
			return generateBlock(6 * blockNum + 1);
		}
	}

	/**
	 * This method is called by calculateBlock(). Given the row numbers, this
	 * method assigns the seat two one of two possible block possibilites. The
	 * inner section will be given preference (i.e. small possible block number),
	 * as the inner seats are more desirable. The out seats (near the edge) are
	 * given the other possible block number. The block number is one of two
	 * numbers, as defined by the input parameter.
	 * 
	 * @param blockNum
	 *           the first possible block number that the seat can be assigned to
	 * @return the block associated with the seat
	 */
	private int generateBlock(int blockNum)
	{
		// Inner seats
		if (number >= 108 && number <= 121)
		{
			return block = blockNum;
			// Outer seats
		} else
		{
			return block = blockNum + 1;
		}
	}

	/**
	 * Getter that gets the seat's row value
	 * 
	 * @return the row value
	 */
	public String getRow()
	{
		return row;
	}

	/**
	 * Getter that gets the seat's number
	 * 
	 * @return the seat's number
	 */
	public int getNumber()
	{
		return number;
	}

	/**
	 * Determines if the seat is available
	 * 
	 * @return boolean that represents if the seat is available or not
	 */
	public boolean isAvailable()
	{
		return available;
	}

	/**
	 * Set the seat's availability status (either true/false)
	 * 
	 * @param available
	 *           the status of the seat
	 */
	public void setAvailable(boolean available)
	{
		this.available = available;
	}

	/**
	 * Get the seat's id value (for priority queue purposes)
	 * 
	 * @return the seat's id value
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * Get the seat's block value (for priority queue purposes)
	 * 
	 * @return the seat's block number
	 */
	public int getBlock()
	{
		return block;
	}

	/**
	 * Returns the handicap status of the seat.
	 * 
	 * @return the seat's handicap status (true/false)
	 */
	public boolean isHandicap()
	{
		return handicap;
	}

	/**
	 * Set the seat's handicap status (true/false)
	 * 
	 * @param handicap
	 *           the seat's new handicap status
	 */
	public void setHandicap(boolean handicap)
	{
		this.handicap = handicap;
	}

	/**
	 * Get the person who is currently occupying the seat (null if no one is
	 * occupying the seat)
	 * 
	 * @return the person currently occupying the seat
	 */
	public Person getPerson()
	{
		return person;
	}

	/**
	 * Define what person is currently in the seat.
	 * 
	 * @param person
	 *           the person who is now in the seat
	 */
	public void setPerson(Person person)
	{
		this.person = person;
	}
}
