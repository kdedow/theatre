package ConcertHall;

import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Assignment 6 driver Implements a theatre seating scheme and creates two
 * ticket offices that reserve tickets for people waiting in a queue line. The
 * theatre is modeled on the Bates Recital Hall.
 * 
 * Karl Dedow and Kevin Rosen 
 * Section: W 10:30-12:00 
 * V1 12.7.2014
 */
public class Driver
{
	// Create the theatre (Bates Recital Hall)
	public static final Theatre THEATRE = new Theatre();

	public static void main(String[] args)
	{
		ConcurrentLinkedQueue<Person> line = new ConcurrentLinkedQueue<Person>();

		// Between 100 and 1000 people in the line, the line is never empty
		Random rng = new Random();
		int lineSize = rng.nextInt(901) + 100;
		for (int i = 1; i <= lineSize; i++)
		{
			// Create a line of people
			line.add(new Person(i));
		}

		// Create our two box offices (which are two unique threads)
		Runnable officeA = new BoxOfficeRunnable(THEATRE, line, "Office A");
		Runnable officeB = new BoxOfficeRunnable(THEATRE, line, "Office B");
		Thread t1 = new Thread(officeA);
		Thread t2 = new Thread(officeB);

		// Start processing
		t1.start();
		t2.start();
	}
}
