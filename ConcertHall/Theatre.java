package ConcertHall;

import java.util.PriorityQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Theatre class This class configures and builds the theatre. The theatre is
 * structured after the Bates concert hall. The theatre is stored as an array of
 * seats and a priority queue (with a comparator) is used to calculate the next
 * best available seat. Also, this class contains methods to determine the next
 * best available seat, to mark the next best available seat as occupied, and to
 * print the reserved seat to the console
 * 
 * Karl Dedow and Kevin Rosen 
 * Section: W 10:30-12:00 
 * V1 12.7.2014
 */
public class Theatre
{
	// status of the seat (handicap vs. regular)
	private final boolean HANDICAP = true;
	private final int THEATRE_SIZE = 694;
	private final int LEFT_ROW = 101;
	private final int RIGHT_ROW = 128;

	private Seat[] seatArray;
	private PriorityQueue<Seat> seatQueue;
	private Lock boxOfficeLock;
	private Lock printLock;

	/**
	 * This constructor implements the seatArray (which is the seat configuration
	 * of the theatre), the seatQueue (which is the priority queue that
	 * determines the next best available seat), and the necessary locks that
	 * will prevent deadlocks.
	 */
	public Theatre()
	{
		seatArray = generateHall();
		seatQueue = buildQueue(seatArray);
		boxOfficeLock = new ReentrantLock();
		printLock = new ReentrantLock();
	}

	/**
	 * This method returns the next best available seat when called. The method
	 * is thread-safe and operates by popping off a value from the seatQueue
	 * priority queue, which is the queue storing all remaining available seats
	 * in order of the next best seat option.
	 * 
	 * @param person
	 *           the person currently reserving a seat
	 * @return the next best available seat or null if no more seats are
	 *         available
	 */
	public Seat bestAvailableSeat(Person person)
	{
		boxOfficeLock.lock();
		try
		{
			if (seatQueue.isEmpty())
			{
				return null;
			} else
			{
				return seatQueue.remove();
			}
		} finally
		{
			boxOfficeLock.unlock();
		}
	}

	/**
	 * This methods marks the inputted seat as unavailable. Essentially, this
	 * method actually reserves the seat for the person in line.
	 * 
	 * @param seat
	 *           the seat to be reserved
	 * @param person
	 *           the person that the seat is being reserved for
	 * @return the newly reserved seat (marked as unavailable)
	 */
	public Seat markAvailableSeatTaken(Seat seat, Person person)
	{
		boxOfficeLock.lock();
		try
		{
			if (seat == null)
			{
				// No more seats left, cannot reserve a seat
				return seat;
			}
			String temp = new String("Row: " + seat.getRow() + " Seat #: "
					+ seat.getNumber());
			person.setWaiting(false);
			person.setSeat(temp);
			// Seat is no longer available, associate the person with the seat
			seat.setAvailable(false);
			seat.setPerson(person);
			// update the status of the seat
			seatArray[seat.getId() - 1] = seat;
			return seat;
		} finally
		{
			boxOfficeLock.unlock();
		}
	}

	/**
	 * This method prints the newly reserved seat to the console. If the theatre
	 * is sold out then indicate that the theatre has sold out.
	 * 
	 * @param seat
	 *           the reserved seat
	 * @param name
	 *           the name of the box office (thread) that reserved the seat
	 */
	public void printTicketSeat(Seat seat, String name)
	{
		printLock.lock();
		try
		{
			if (seat == null)
			{
				// Sold out
				System.out.println(name);
				System.out.println("No more seats available -> SOLD OUT\n");
				return;
			}
			// Output the ticket
			System.out.println(name + "\nSeat Reserved for Tonight's Show:");
			System.out.println("Row: " + seat.getRow() + ", Seat: "
					+ seat.getNumber());
			System.out.println("Customer Name/ID: " + seat.getPerson().getId());
			System.out.println();
		} finally
		{
			printLock.unlock();
		}
	}

	/**
	 * This method generates the theatre configuration. Modeled after Bates
	 * Recital Hall
	 * 
	 * @return the array of seats (which is the theatre configuration)
	 */
	private Seat[] generateHall()
	{
		int id = 1;
		// 694 seats in the theatre
		Seat[] seatArray = new Seat[THEATRE_SIZE];

		// Generate the bates concert hall scheme
		char row = 'A';
		String rowString = String.valueOf(row);
		for (int i = 104; i <= 125; i++)
		{
			// Generate the first row
			seatArray[id - 1] = new Seat(rowString, i, !HANDICAP, id);
			id++;
		}
		row += 1;
		rowString = String.valueOf(row);
		// Generate row B-Z
		while (row <= 'Z')
		{
			// No rows I or O -> skip these letters
			if (row == 'I' || row == 'O')
			{
				row += 1;
				rowString = String.valueOf(row);
			}
			for (int i = LEFT_ROW; i <= RIGHT_ROW; i++)
			{
				// Generate the seats and store them in the array
				seatArray[id - 1] = new Seat(rowString, i, !HANDICAP, id);
				id++;
			}
			row += 1;
			rowString = String.valueOf(row);
		}
		// Generate the last row -> AA
		rowString = "AA";
		for (int i = LEFT_ROW; i <= RIGHT_ROW; i++)
		{
			// 105-110 and 119-124 are handicap
			// Defining the handicap
			if ((i >= 105 && i <= 110) || (i >= 119 && i <= 124))
			{
				// Handicap spots
				seatArray[id - 1] = new Seat(rowString, i, HANDICAP, id);
				id++;
			} else
			{
				// Available seats, not handicap spots
				seatArray[id - 1] = new Seat(rowString, i, !HANDICAP, id);
				id++;
			}
		}
		return seatArray;
	}

	/**
	 * This method generates the priority queue of available seats. The queue
	 * uses a comparator that determines the priority of each seat. The queue is
	 * ordered by next best available seat. The next best available seat is
	 * popped of the queue first and so on. To determine the next best available
	 * seat the theatre was split into block of seats that indicate the overall
	 * value of the seat. In each block, seats are filled in ascending order.
	 * Also, handicap seats are not added to the queue as they are not available.
	 * 
	 * @param seatArray
	 *           the array of seats (or theatre configuration)
	 * @return the priority queue of available seats
	 */
	private PriorityQueue<Seat> buildQueue(Seat[] seatArray)
	{
		PriorityQueue<Seat> seatQueue = new PriorityQueue<Seat>(THEATRE_SIZE,
				new SeatComparator());
		// loop through all
		for (int i = 0; i < seatArray.length; i++)
		{
			// Only add seats that are not handicap to the seat queue
			// Also the seat must be available, cannot add unavailable seats
			if (!seatArray[i].isHandicap() && seatArray[i].isAvailable())
			{
				seatQueue.offer(seatArray[i]);
			}
		}
		return seatQueue;
	}

	/**
	 * Getter that returns the seatArray
	 * 
	 * @return the seatArray (or theatre configuration)
	 */
	public Seat[] getSeatArray()
	{
		return seatArray;
	}

	/**
	 * Getter that returns the priority queue (seatQueue)
	 * 
	 * @return the priority queue of available seats
	 */
	public PriorityQueue<Seat> getSeatQueue()
	{
		return seatQueue;
	}
}
