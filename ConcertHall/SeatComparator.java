package ConcertHall;

import java.util.Comparator;

/**
 * The SeatComparator class This class implements the Comparator interface and
 * defines a compare method (which overrides the compare method in the
 * Comparator interface). The compare method ensures that the priority queue
 * currently available seats correctly returns the next best option (based on
 * the id of the seat, as well as the block the seat is a part of).
 * 
 * Karl Dedow and Kevin Rosen 
 * Section: W 10:30-12:00 
 * V1 12.7.2014
 */
public class SeatComparator implements Comparator<Seat>
{
	/**
	 * This method returns an int based on the block numbers of both seats, as
	 * well as the id of both seats. First, the method compares the block value
	 * of each respective seat. If the block values are equal (meaning both seats
	 * are in the same block) then the IDs of the respective seats are compared.
	 * The seat with smaller id (which is the more preferable seat) is then
	 * guaranteed to take precedence in the priority queue (again as it is the
	 * better seat) by the method returning either a negative value or positive
	 * value (based on the comparison). If the block values are not equal then
	 * the are compared instead of the IDs. The seat with the smaller block value
	 * (which is the better seat) is then guaranteed to take precedence in the
	 * priority queue (again is it is the better seat) by the method returning
	 * either a negative value or positive value (based on the comparison).
	 * 
	 * @param seat1
	 *           a seat that is to be added to the priority queue
	 * @param seat2
	 *           a seat in the queue
	 * @return an int that indicates to the priority queue which seat should take
	 *         precedence
	 */
	public int compare(Seat seat1, Seat seat2)
	{
		// Either compare the id values or the block values. If the block values
		// are not equal to each other, then the block values should be compared
		// If the block values are equal to each other, then the id values should
		// be compared
		return (seat1.getBlock() == seat2.getBlock()) ? (Integer.valueOf(seat1
				.getId()).compareTo(seat2.getId())) : (Integer.valueOf(seat1
				.getBlock()).compareTo(seat2.getBlock()));
	}
}
