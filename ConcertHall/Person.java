package ConcertHall;

/**
 * The Person class This class defines a person who initially will be waiting in
 * line in order to get into the theatre.
 * 
 * Karl Dedow and Kevin Rosen 
 * Section: W 10:30-12:00 
 * V1 12.7.2014
 */
public class Person
{
	private int id;
	private String seat;
	private boolean waiting;

	/**
	 * The constructor for the person class. The constructor assigns an id to the
	 * person. The id is useful for tracking purposes. In this way, we can
	 * determine if the persons waiting in line are getting processed
	 * sequentially. The constructor also contains a seat string and a waiting
	 * boolean. Initially, the person is waiting in line, so waiting is set to
	 * true. Once the person is assigned a seat the waiting is set to false and
	 * the seat string is set to the person's row and seat number.
	 * 
	 * @param id
	 *           - the id of the person (for internal purposes of tracking
	 *           people)
	 */
	public Person(int id)
	{
		this.id = id;
		seat = null;
		waiting = true;
	}

	/**
	 * Get the person's Id
	 * 
	 * @return the person's Id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * Get the person's seat row and number
	 * 
	 * @return the string that represents the person's row and seat number
	 */
	public String getSeat()
	{
		return seat;
	}

	/**
	 * Set the person's row and seat number
	 * 
	 * @param seat
	 *           the string that represents the person's seat row and number
	 */
	public void setSeat(String seat)
	{
		this.seat = seat;
	}

	/**
	 * Returns a boolean that represents the person's waiting status. Returns
	 * true if the person is waiting and false if the person has obtained a seat
	 * in the theatre.
	 * 
	 * @return the person's waiting status (true/false)
	 */
	public boolean isWaiting()
	{
		return waiting;
	}

	/**
	 * Set the person's waiting status. True if the person is currently waiting
	 * in line and false if the person has obtained a seat in the theatre
	 * 
	 * @param waiting
	 *           the person's new waiting status (true/false)
	 */
	public void setWaiting(boolean waiting)
	{
		this.waiting = waiting;
	}

}
